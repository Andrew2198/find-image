package find_image.findimage.Support;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import find_image.findimage.R;

public class Adapter_list extends RecyclerView.Adapter<Adapter_list.ViewHolder> {
    private List<Realm_class> listInfo=new ArrayList<>();
    private Context mContext;



    public Adapter_list(Context mContext, List<Realm_class> listInfo) {
        this.listInfo = listInfo;
        this.mContext = mContext;
    }


    @Override
    public Adapter_list.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Glide.with(mContext).load(listInfo.get(position).getPathImage()).into(holder.imgImage);
        holder.tvName.setText(listInfo.get(position).getText());
        }

    @Override
    public int getItemCount() {
        return listInfo.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvName;
       public ImageView imgImage;


        public ViewHolder(View itemView) {
            super(itemView);
            tvName=(TextView)itemView.findViewById(R.id.tv_text);
            imgImage=(ImageView) itemView.findViewById(R.id.img_image);





        }
    }
}
