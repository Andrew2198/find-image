package find_image.findimage.Support;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Realm_class extends RealmObject {
    @PrimaryKey
    private String pathImage;
    private String text;

    public Realm_class(){

    }

    public Realm_class(String url, String word) {
        this.pathImage=url;
        this.text=word;
    }


    public String getPathImage() {
        return pathImage;
    }

    public void setPathImage(String pathImage) {
        this.pathImage = pathImage;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
