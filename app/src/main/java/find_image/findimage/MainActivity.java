package find_image.findimage;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import find_image.findimage.Support.Adapter_list;
import find_image.findimage.Support.Realm_class;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.edt_text)
    EditText enterText;
    @BindView(R.id.rec_view)
    RecyclerView recyclerView;
    private Adapter_list adapterList;
    private List<Realm_class> list = new ArrayList<>();
    private Realm mRealm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        RealmConfiguration config = new RealmConfiguration.Builder(this).deleteRealmIfMigrationNeeded().build();
        Realm.setDefaultConfiguration(config);
        mRealm = Realm.getDefaultInstance();

        list = mRealm.where(Realm_class.class).findAll();
        adapterList = new Adapter_list(this, list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapterList);

        enterText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if ((keyEvent != null && (keyEvent.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (i == EditorInfo.IME_ACTION_DONE)) {
                    if (!enterText.getText().toString().trim().equals("")) {
                        loadDataList(enterText.getText().toString().trim());
                        enterText.setText("");
                    } else {
                        Toast.makeText(MainActivity.this, "Enter word...", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });
    }

    public void loadDataList(final String word) {
        try {
            AndroidNetworking.get("http://api.giphy.com/v1/gifs/search?q={word}&api_key=VHQPiA9jPSA57VIPns5HtdsHZFyCJOJY")
                    .addPathParameter("word", word)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray data = response.getJSONArray("data");
                                if (data.length() != 0) {
                                    JSONObject item = data.getJSONObject(0);
                                    JSONObject image = item.getJSONObject("images");
                                    JSONObject fixedHight = image.getJSONObject("fixed_height_still");
                                    String url = fixedHight.getString("url");
                                    Realm_class object = new Realm_class();
                                    object.setPathImage(url);
                                    object.setText(word);
                                    mRealm.beginTransaction();
                                    mRealm.copyToRealmOrUpdate(object);
                                    mRealm.commitTransaction();
                                    list = mRealm.where(Realm_class.class).findAll();
                                    adapterList.notifyDataSetChanged();
                                    } else {
                                    Toast.makeText(MainActivity.this, "Error... No image...", Toast.LENGTH_SHORT).show();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("LOG", String.valueOf(anError));
                            }
                    });

        } catch (Exception e) {
            Log.i("TAG", String.valueOf(e));

        }


    }


}
